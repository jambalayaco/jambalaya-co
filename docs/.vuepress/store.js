import Vue from 'vue';
import Vuex from 'vuex';
import FM from 'front-matter';
import { SITE_DATA } from '@dynamic/siteData'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isEditing: false,
    siteData: SITE_DATA,
    theme: 'Light',
  },
  getters: {
    getMatterById: (state) => (id) => {
      let matter = { 
        hasContent: false, 
        frontmatter: {fields: {theme:{label: "Theme", options: ["Light", "Dark"], type: "select", value: "Dark"}}}
      };
      let data = {}
      data = state.siteData[id];
      if (data) {
        const front = FM(data.data);
        matter.hasContent = true;
        matter.frontmatter = front.attributes;
        matter.content = front.body;
      } else {
        //console.log('No data')
      }
      return matter;
    }
  },
  mutations: {
    onEditUpdate(state, update) {
      let formatedUpdate = {};
      Object.keys(update).forEach(function (key) {
        formatedUpdate[key] = {data: update[key]}
      });
      state.siteData = Object.assign({}, state.siteData, formatedUpdate);
    },
    toggleEdit(state) {
      state.isEditing = !state.isEditing;
    },
    setTheme(state, theme) {
      state.theme = theme;
    }
  }
});