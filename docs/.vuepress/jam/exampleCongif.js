const fs = require('fs');
const path = require('path');

const otherContent = require('../../content/content.json');
let siteData = {cat: 'hat'};

module.exports = (options, context) => ({
  async additionalPages (context) {
    let pages = await getPages();
    console.log('add pages', context)
    return pages;
  },
  clientDynamicModules() {
    console.log('CDM');
    return {}
  },
  title: 'Jambalaya - Coming soon',
  description: 'Jambalaya - coming soon, a better way to build sites',
  base: '/',
  dest: 'public',
  mike: 'Super Awsome',
  themeConfig: {
    navbar: false,
    siteData: getSiteData(),
  },
  head: [
    ['link', { rel: 'stylesheet', href: '/jam/css/jambalaya.css' }]
  ],
})

async function getPages() {
  let pages = [];
  let fileList;

  // Load pages
  fileList = await walk('./content/pages');
  let pagesData = {}
  await Promise.all(fileList.map(async filePath => {
    let content = await loadFile(filePath);
    siteData[filePath] = { data: content }
    pages.push({path: filePath, content })
  }))

  // Load additional pages
  Object.keys(otherContent.pages).forEach(key => {
    pages.push(otherContent.pages[key]);
  })

  // Load components
  fileList = await walk('./content/pages');
  await Promise.all(fileList.map(async filePath => {
    let content = await loadFile(filePath);
    siteData[filePath] = { data: content }
  }))  

  return pages
}

function getSiteData() {
  console.log("get that data", siteData);
  return siteData
}

async function loadFile(filePath) {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, 'utf8', function (err, content) {
      resolve(content);
    });
  });
}

async function walk(dir) {
  return new Promise((resolve, reject) => {
    fs.readdir(dir, (error, files) => {
      if (error) {
        return reject(error);
      }
      Promise.all(files.map((file) => {
        return new Promise((resolve, reject) => {
          const filepath = path.join(dir, file);
          fs.stat(filepath, (error, stats) => {
            if (error) {
              return reject(error);
            }
            if (stats.isDirectory()) {
              walk(filepath).then(resolve);
            } else if (stats.isFile()) {
              resolve(filepath);
            }
          });
        });
      }))
        .then((foldersContents) => {
          resolve(foldersContents.reduce((all, folderContents) => all.concat(folderContents), []));
        });
    });
  });
}



