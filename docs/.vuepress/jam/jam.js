export { loadJambalayaEditor };

function loadJambalayaEditor() {
	if (typeof window !== 'undefined') {
		document.getElementById('jambalaya')
		var jamExists = document.getElementById("jambalaya-editor");
		if (!jamExists) {
			var node = document.createElement("jambalaya-editor");
			node.setAttribute("id", "jambalaya-editor");
			document.getElementById('jambalaya').appendChild(node);
			import('./components/jambalaya-editor');
		}
	}
}