const fs = require('fs');
const path = require('path');

const otherContent = require('../../content/content.json');
let siteData = {};

module.exports = (options, ctx) => ({
  async additionalPages (context) {
    let pages = await getPages();
    return pages;
  },
  async clientDynamicModules() {
    let files = await getSiteData();
    return {
      name: 'siteData.js',
      content: `export const SITE_DATA = ${JSON.stringify(siteData)}`
    }
  },
  title: 'Jambalaya',
  description: 'Jambalaya - coming soon, a better way to build sites',
  base: '/',
  dest: 'public',
  head: [
    ['meta', { name: 'viewport', content: 'width=device-width, initial-scale=1' }],
  ],
})

async function getPages() {
  let pages = [];
  let fileList;

  // Load pages
  fileList = await walk('./content/pages');
  let pagesData = {}
  await Promise.all(fileList.map(async filePath => {
    let content = await loadFile(filePath);
    siteData[filePath] = { data: content }
    let filePathFormated = filePath.replace("content/pages", "");
    filePathFormated = filePathFormated.replace("index.md", "");
    filePathFormated = filePathFormated.replace(".md", "/");
    pages.push({ 
      path: filePathFormated, 
      content, 
      frontmatter: {
        id: filePath
      }
     })
  }))

  // Load additional pages
  Object.keys(otherContent.pages).forEach(key => {
    pages.push(otherContent.pages[key]);
  })

  return pages
}

async function getSiteData() {
  let fileList;

  // Load components
  fileList = await walk('./content/components');
  await Promise.all(fileList.map(async filePath => {
    let content = await loadFile(filePath);
    siteData[filePath] = { data: content }
  }))

  return
}

async function loadFile(filePath) {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, 'utf8', function (err, content) {
      resolve(content);
    });
  });
}

async function walk(dir) {
  return new Promise((resolve, reject) => {
    fs.readdir(dir, (error, files) => {
      if (error) {
        return reject(error);
      }
      Promise.all(files.map((file) => {
        return new Promise((resolve, reject) => {
          const filepath = path.join(dir, file);
          fs.stat(filepath, (error, stats) => {
            if (error) {
              return reject(error);
            }
            if (stats.isDirectory()) {
              walk(filepath).then(resolve);
            } else if (stats.isFile()) {
              let str = filepath.replace(/\\/g, '/');
              resolve(str);
            }
          });
        });
      }))
        .then((foldersContents) => {
          resolve(foldersContents.reduce((all, folderContents) => all.concat(folderContents), []));
        });
    });
  });
}



