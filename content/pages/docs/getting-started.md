---
fields:
  layout:
    type: select
    label: Page layout
    value: Docs
    options:
      - Default
      - Docs
      - Home
---
# Getting Started
Jambalaya is available through CDN and NPM. 

## CDN
Use the following script to add Jambalaya directly to your website: 

```
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.11"></script>
```
## NPM
If you're using a module bundler, like Webpack or Browserify, Jambalaya can be installed with the following NPM command:

```
$ npm install jambalaya-co
```


