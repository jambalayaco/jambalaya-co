---
fields:
  layout:
    type: select
    label: Page layout
    value: Docs
    options:
      - Default
      - Docs
      - Home
  title:
    type: input
    label: Page Title
    value: Config
---


# Adding Jambalaya
Jambalaya can be added to your page as a container or as a web component.  

## Container
Include a div with the id `jambalaya` in your project template that includes a `data-cms` attribute and a `data-project` attribute ([see config](/config)).  This will be the container Jambalaya is loaded into.

```
<div id="jambalaya" data-cms=”gitlab” data-project="16728015"></div>
```


This method can help speed up page load times by only loading the resources as needed.  To use, include the following script and attach it to an edit button action.  Jambalaya will create a web component and add it to the container you provided.

```
import { loadJambalayaEditor } from "../jam/jam";
```

## Web component 
Jambalaya can be added directly to a site as a custom web component.  Include the following script and web component on your page.
```
import "../jam/components/jambalaya-editor";
```
```
<div id="jambalaya" data-cms=”gitlab” data-project="16728015">
	<jambalaya-editor />
</div>

```
