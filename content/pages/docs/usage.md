---
fields:
  layout:
    type: select
    label: Page layout
    value: Docs
    options:
      - Default
      - Docs
      - Home
---

# Usage
You can use Jambalaya to edit pages, components, or anything that is markdown based.

## Define editable elements
To make content editable, include the `data-jambalaya` data attribute and the content id of the associated markdown file.
Jambalaya will listen to click events on anything `data-jambalaya` and will load the associated file to be edited.

```
<button  data-jambalaya="contentId">Edit</button>
```

## Listen for changes
To preview your changes, listen for `jambalaya-events` in your template.  As changes are made, Jambalaya emits an event including an object containing all the changes to the files.

```
if (typeof document !== 'undefined') {
  document.addEventListener("jambalaya-event", this.onJambalayaEvent);
}
```

<div class="tip">
  <span class="tip-title">Tip</span>
  To prevent errors when generating files with a static site generator (SSG), it's recommended to check first before referencing <code>document</code>.
</div>

