---
fields:
  layout:
    type: select
    label: Page layout
    value: Docs
    options:
      - Default
      - Docs
      - Home
  title:
    type: input
    label: Page Title
    value: Config
---
# Config options

## CMS
Jambalaya works with the following content management systems:

  ### Gitlab
    - "cms": "gitlab"
  ### Github
    - "cms": "github"
  ### Bitbucket 
  Coming soon.
  ### Contentful
  Coming soon.
  
## Project ID
The project id is based on the CMS for your site.  The format varies depending on the provider.

### GitLab
For Gitlab, use the `Project ID` associated with your project.  
```
  "projectId": "12345678"
```
### Github
For Github, use the url for your project including the owner and repository (:owner/:repo).
```
  "projectId": "uxuimike/mirror-test"
```

## Demo
Demo lets you edit and preview changes to your site without having to log-in.  Saving is disabled while in demo, but you can log-in to enable saving.


<div class="tip">
  <span class="tip-title">Tip</span>
  For Demo mode to work, the project needs to be public.
</div>


