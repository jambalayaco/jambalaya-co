---
fields:
  layout:
    type: select
    label: Page layout
    value: Docs
    options:
      - Default
      - Docs
      - Home
  title:
    type: input
    label: Page Title
    value: Config
---
# Configure
To configure Jambalaya for your project, include a div with the id `jambalaya` in your project template. To work, Jambalaya needs the CMS and the project id. These can either be passed inline or via config file.  

## Inline
Include a div with the id `jambalaya` in your project template that includes a `data-cms` attribute and a `data-project` attribute. 

### Html
```
<div id="jambalaya" data-cms=”xxxxxx” data-project="yyyyyy"></div>
```

## Config file
In addition to the CMS and the project id, the config file method allows you to set additional options. Include a div with the id `jambalaya` in your project template that includes a `data-config` attribute with the path to your configuration file. 

### Html
```
<div id="jambalaya" data-config="/jamconfig.json"></div>
``` 

### jamconfig.json
```
{
  "projectId": "12345678",
  "cms": "gitlab",
  "demo": true
}
```