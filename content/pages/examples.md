---
home: true
fields:
  layout:
    type: select
    label: Page layout
    value: Default
    options:
      - Default
      - Docs
      - Home
  title:
    type: input
    label: Page Title
    value: Jambalaya
  theme:
    type: select
    label: Theme
    value: Dark
    options:
      - Light
      - Dark
---
# Examples

## Coming soon
