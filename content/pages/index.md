---
home: true
fields:
  layout:
    type: select
    label: Page layout
    value: Home
    options:
      - Default
      - Docs
      - Home
  title:
    type: input
    label: Page Title
    value: Jambalaya
  theme:
    type: select
    label: Theme
    value: Dark
    options:
      - Light
      - Dark
  herotitle:
    type: input
    label: Hero Title
    value: A Better Way to Build Better Sites
---
Wish you could build faster, more secure, scalable sites without all the complexity? We did. So we built Jambalaya, a set of tools to simplify creating, editing and deploying JAMstack sites. With Jambalaya, manage your content where it makes the most sense, directly on your site.




