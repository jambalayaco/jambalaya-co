---
home: true
fields:
  title:
    type: input
    label: Title
    value: Connect 
  align:
    type: radio
    label: Align
    value: Left
    options:
      - Left
      - Center
      - Right
---
With planned support for a variety of the most popular content management systems, CDN’s, CI/CD tools, and hosting providers, Jambalaya will let you manage your entire site, all without having to leave the page.  
