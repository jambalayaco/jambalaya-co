---
home: true
fields:
  title:
    type: input
    label: Title
    value: Open Source
  align:
    type: radio
    label: Align
    value: Left
    options:
      - Left
      - Center
      - Right
---
Jambalaya is a community-driven open-source project.  We would love your support. Open-source contributors help build, maintain, and drive the future of the project. There are many ways to get involved, tackle an issue, fix a bug, contribute a feature, or suggest one.   You can support the Jambalaya open-source project by becoming a backer.
