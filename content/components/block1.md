---
fields:
  title:
    type: input
    label: Title
    value: Simple
---
With Jambalaya, if you can save a document, you can create a website.  There is no need to learn a CMS, or manage a complicated setup, just edit your site using your site.  A simple UX with in-page editing, live preview, and a connected workflow, Jambalaya makes editing fast and easy.   

