---
home: true
fields:
  title:
    type: input
    label: Title
    value: On-page editing
  image:
    type: input
    label: Image
    value: /img/on-page.svg
  alt:
    type: input
    label: Alt tag
    value: On-page editing
  background:
    type: input
    label: Background Image
    value: /img/dots.svg
  align:
    type: radio
    label: Image on
    value: Left
    options:
      - Left
      - Right
  cta:
    type: input
    label: CTA
    value: Learn more
  ctaurl:
    type: input
    label: CTA Url
    value: /docs/usage
---
Edit your content directly on your website.  Jambalaya is a bridge between your content and website template.  You can make just about anything on your site editable: pages, components, or all things markdown based.  Define the fields you want in frontmatter and Jambalaya will pick them up and make them editable, unlocking the ability to customize your template.  
