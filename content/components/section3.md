---
home: true
fields:
  title:
    type: input
    label: Title
    value: Git workflow
  image:
    type: input
    label: Image
    value: /img/jamgit.svg
  alt:
    type: input
    label: Alt tag
    value: Block
  background:
    type: input
    label: Background Image
    value: /img/dot-gradient.svg
  align:
    type: radio
    label: Image on
    value: Right
    options:
      - Left
      - Right
  cta:
    type: input
    label: CTA
    value: Learn more
  ctaurl:
    type: input
    label: CTA Url
    value: /docs/usage
---
Leverage the benefits of versioning, branching, and source control by managing your content with Git. Store your content alongside your code or in a separate repository. Jambalaya connects Git to your site and creates a seamless editing experience. 

