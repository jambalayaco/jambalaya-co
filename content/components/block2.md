---
fields:
  title:
    type: input
    label: Title
    value: Fast
---
Jambalaya is built for JAMstack allowing you to unlock the performance, security, and scalability benefits that come with it. A developer experience focused API makes it simple to integrate with your favorite static site generator. 
