---
fields:
  copywrite:
    type: input
    label: Copywrite
    value: © 2020 Jambalaya-co
---
- Docs
- [Get Started](/docs/getting-started) 
- [Add ](/docs/add-to-site)
- [Config](/docs/config)


- Examples
- [Gatsby](/examples)
- [Vuepress](/examples)
- [Nuxt](/examples)
- [Next](/examples)


- Contribute
- [Get involved](/contribute)
- [Backers](/contribute)


