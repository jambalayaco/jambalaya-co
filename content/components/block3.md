---
fields:
  title:
    type: input
    label: Title
    value: Flexible
---
Customize as needed to fit your site, your CMS, and your organization.  Bring the CMS of your choice and easily integrate it. Or if you prefer, Jambalaya also supports a Git workflow for managing content. Jambalaya also plays nice with other technologies, even plain HTML. 
